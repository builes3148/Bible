﻿namespace Bible.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Reflection;
    using Models;
    using Services;
    using Xamarin.Forms;

    public class BiblesViewModel : BaseViewModel
    {
        #region Services
        private ApiService apiService;
        #endregion

        #region Attributes
        private BiblesResult biblesResult;
        private List<BaseBible> myBibles;
        private ObservableCollection<BibleItemViewModel> bibles;
        private bool isRefreshing;
        #endregion

        #region Properties
        public ObservableCollection<BibleItemViewModel> Bibles
        {
            get { return this.bibles; }
            set { SetValue(ref this.bibles, value); }
        }

        public bool IsRefreshing
        {
            get { return this.isRefreshing; }
            set { SetValue(ref this.isRefreshing, value); }
        }
        #endregion

        #region Constructors
        public BiblesViewModel()
        {
            this.apiService = new ApiService();
            this.LoadBibles();
        }
        #endregion

        #region Methods
        private async void LoadBibles()
        {
            this.IsRefreshing = true;

            var connection = await this.apiService.CheckConnection();
            if (!connection.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    "Error", 
                    connection.Message, 
                    "Accept");
                return;
            }

            var response = await this.apiService.Get<BiblesResult>(
                "http://api.biblesupersearch.com", 
                "/api", 
                "/bibles");
            
            if (!response.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    response.Message,
                    "Accept");
                return;
            }

            this.biblesResult = (BiblesResult)response.Result;

            var type = typeof(Bibles);
            var properties = type.GetRuntimeFields();
            this.myBibles = new List<BaseBible>();

            foreach (var property in properties)
            {
                var code = property.Name.Substring(1, 3);
                var bible = (BaseBible)property.GetValue(this.biblesResult.Results);
                this.myBibles.Add(bible);
            }

            this.Bibles = new ObservableCollection<BibleItemViewModel>(
                this.ToBibleItemViewModel());
            this.IsRefreshing = false;
        }

        private IEnumerable<BibleItemViewModel> ToBibleItemViewModel()
        {
            return this.myBibles.Select(b => new BibleItemViewModel
            {
                CopyRight = b.CopyRight,
                Italics = b.Italics,
                Lang = b.Lang,
                LangShort = b.LangShort,
                Module = b.Module,
                Name = b.Name,
                Rank = b.Rank,
                Research = b.Research,
                ShortName = b.ShortName,
                Strongs = b.Strongs,
                Year = b.Year,
           });       
        }
        #endregion
    }
}