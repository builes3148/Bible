﻿namespace Bible.ViewModels
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Reflection;
    using Models;
    using Services;
    using Xamarin.Forms;

    public class BookViewModel : BaseViewModel
    {
        #region Services
        private ApiService apiService;
        #endregion

        #region Attributes
        private Book book;
        private bool isRefreshing;
        private Content content;
        private List<BaseVerse> myVerses;
        private ObservableCollection<BaseVerse> verses;
        #endregion

        #region Properties
        public bool IsRefreshing
        {
            get { return this.isRefreshing; }
            set { SetValue(ref this.isRefreshing, value); }
        }

        public ObservableCollection<BaseVerse> Verses
        {
            get { return this.verses; }
            set { SetValue(ref this.verses, value); }
        }
        #endregion

        #region Constructors
        public BookViewModel(Book book)
        {
            this.apiService = new ApiService();
            this.book = book;
            this.LoadContent();
        }
        #endregion

        #region Methods
        private async void LoadContent()
        {
            this.IsRefreshing = true;

            var connection = await this.apiService.CheckConnection();
            if (!connection.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    connection.Message,
                    "Accept");
                return;
            }

            var response = await this.apiService.Get<Content>(
                "http://api.biblesupersearch.com",
                "/api",
                string.Format(
                    "?bible={0}&reference={1}", 
                    MainViewModel.GetInstance().SelectedModule, 
                    this.book.ShortName));

            if (!response.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    response.Message,
                    "Accept");
                return;
            }

            this.content = (Content)response.Result;
            var contentResult = content.ContentResults[0];

            var type = typeof(Bibles);
            var properties = type.GetRuntimeFields();
            BaseBible bible = null;

            foreach (var property in properties)
            {
                bible = (BaseBible)property.GetValue(contentResult.Verses);
                if (bible != null)
                {
                    break;
                }
            }

            if (bible == null)
            {
                return;
            }

            type = typeof(BaseBible);
            properties = type.GetRuntimeFields();
            Chapter chapter = null;

            foreach (var property in properties)
            {
                try
                {
                    chapter = (Chapter)property.GetValue(bible);
                }
                catch
                {
                    chapter = null;
                }

                if (chapter != null)
                {
                    break;
                }
            }

            type = typeof(Chapter);
            properties = type.GetRuntimeFields();
            this.myVerses = new List<BaseVerse>();
            foreach (var property in properties)
            {
                var verse = (BaseVerse)property.GetValue(chapter);
                if (verse != null)
                {
                    this.myVerses.Add(verse);
                }
            }

            this.Verses = new ObservableCollection<BaseVerse>(this.myVerses);
            this.IsRefreshing = false;
        }
        #endregion
    }
}