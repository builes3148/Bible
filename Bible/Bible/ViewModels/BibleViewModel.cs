﻿namespace Bible.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Models;
    using Services;
    using Xamarin.Forms;

    public class BibleViewModel : BaseViewModel
    {
        #region Services
        private ApiService apiService;
        #endregion

        #region Attributes
        private BaseBible baseBible;
        private bool isRefreshing;
        private BooksResult booksResult;
        private ObservableCollection<BookItemViewModel> books;
        #endregion

        #region Properties
        public bool IsRefreshing
        {
            get { return this.isRefreshing; }
            set { SetValue(ref this.isRefreshing, value); }
        }

        public ObservableCollection<BookItemViewModel> Books
        {
            get { return this.books; }
            set { SetValue(ref this.books, value); }
        }
        #endregion

        #region Constructors
        public BibleViewModel(BaseBible baseBible)
        {
            this.apiService = new ApiService();
            this.baseBible = baseBible;
            this.LoadBooks();
        }
        #endregion

        #region Methods
        private async void LoadBooks()
        {
            this.IsRefreshing = true;

            var connection = await this.apiService.CheckConnection();
            if (!connection.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    connection.Message,
                    "Accept");
                return;
            }

            var response = await this.apiService.Get<BooksResult>(
                "http://api.biblesupersearch.com",
                "/api",
                string.Format("/books?language={0}", baseBible.LangShort));

            if (!response.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    response.Message,
                    "Accept");
                return;
            }

            this.booksResult = (BooksResult)response.Result;

            if (baseBible.LangShort != "en")
            {
                var response2 = await this.apiService.Get<BooksResult>(
                    "http://api.biblesupersearch.com",
                    "/api",
                    "/books?language=en");

                if (!response2.IsSuccess)
                {
                    this.IsRefreshing = false;
                    await Application.Current.MainPage.DisplayAlert(
                        "Error",
                        response2.Message,
                        "Accept");
                    return;
                }

                var booksResult2 = (BooksResult)response2.Result;

                for (int i = 0; i < this.booksResult.Books.Count; i++)
                {
                    this.booksResult.Books[i].ShortName = booksResult2.Books[i].ShortName;
                }
            }

            this.Books = new ObservableCollection<BookItemViewModel>(
                this.ToBookItemViewModel());
            this.IsRefreshing = false;
        }

        private IEnumerable<BookItemViewModel> ToBookItemViewModel()
        {
            return this.booksResult.Books.Select(b => new BookItemViewModel
            {
                Id = b.Id,
                Name = b.Name,
                ShortName = b.ShortName,
           });       
        }
        #endregion
    }
}