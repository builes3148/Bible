﻿namespace Bible.Models
{
    using Newtonsoft.Json;

    public class BiblesResult
    {
        [JsonProperty(PropertyName = "error_level")]
        public int ErrorLevel { get; set; }

        [JsonProperty(PropertyName = "results")]
        public Bibles Results { get; set; }
    }
}