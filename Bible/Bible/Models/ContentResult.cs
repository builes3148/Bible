﻿using System;
using Newtonsoft.Json;

namespace Bible.Models
{
    public class ContentResult
    {
        [JsonProperty(PropertyName = "book_id")]
        public int BookId { get; set; }

        [JsonProperty(PropertyName = "book_name")]
        public string BookName { get; set; }

        [JsonProperty(PropertyName = "book_short")]
        public string BookShort { get; set; }

        [JsonProperty(PropertyName = "book_raw")]
        public string BookRaw { get; set; }

        [JsonProperty(PropertyName = "chapter_verse")]
        public string ChapterVerse { get; set; }

        [JsonProperty(PropertyName = "chapter_verse_raw")]
        public object ChapterVerseRaw { get; set; }

        [JsonProperty(PropertyName = "verses")]
        public Verses Verses { get; set; }

        [JsonProperty(PropertyName = "verses_count")]
        public int VersesCount { get; set; }

        [JsonProperty(PropertyName = "single_verse")]
        public bool SingleVerse { get; set; }

        [JsonProperty(PropertyName = "nav")]
        public Nav Nav { get; set; }
    }
}