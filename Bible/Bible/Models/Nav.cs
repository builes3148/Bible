﻿namespace Bible.Models
{
    using Newtonsoft.Json;

    public class Nav
    {
        [JsonProperty(PropertyName = "prev_book")]
        public string PrevBook { get; set; }

        [JsonProperty(PropertyName = "next_book")]
        public string NextBook { get; set; }

        [JsonProperty(PropertyName = "next_chapter")]
        public string NextChapter { get; set; }

        [JsonProperty(PropertyName = "prev_chapter")]
        public string PrevChapter { get; set; }
    }
}
