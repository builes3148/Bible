﻿namespace Bible.Models
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public class BooksResult
    {
        [JsonProperty(PropertyName = "error_level")]
        public int error_level { get; set; }

        [JsonProperty(PropertyName = "results")]
        public List<Book> Books { get; set; }
    }
}