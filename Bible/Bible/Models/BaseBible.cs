﻿namespace Bible.Models
{
    using Newtonsoft.Json;

    public class BaseBible
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "shortname")]
        public string ShortName { get; set; }

        [JsonProperty(PropertyName = "module")]
        public string Module { get; set; }

        [JsonProperty(PropertyName = "year")]
        public string Year { get; set; }

        [JsonProperty(PropertyName = "lang")]
        public string Lang { get; set; }

        [JsonProperty(PropertyName = "lang_short")]
        public string LangShort { get; set; }

        [JsonProperty(PropertyName = "copyright")]
        public int CopyRight { get; set; }

        [JsonProperty(PropertyName = "italics")]
        public int Italics { get; set; }

        [JsonProperty(PropertyName = "strongs")]
        public int Strongs { get; set; }

        [JsonProperty(PropertyName = "rank")]
        public int Rank { get; set; }

        [JsonProperty(PropertyName = "research")]
        public int Research { get; set; }

        [JsonProperty(PropertyName = "1")]
        public Chapter Chapter1 { get; set; }

        [JsonProperty(PropertyName = "2")]
        public Chapter Chapter2 { get; set; }

        [JsonProperty(PropertyName = "3")]
        public Chapter Chapter3 { get; set; }

        [JsonProperty(PropertyName = "4")]
        public Chapter Chapter4 { get; set; }

        [JsonProperty(PropertyName = "5")]
        public Chapter Chapter5 { get; set; }

        [JsonProperty(PropertyName = "6")]
        public Chapter Chapter6 { get; set; }

        [JsonProperty(PropertyName = "7")]
        public Chapter Chapter7 { get; set; }

        [JsonProperty(PropertyName = "8")]
        public Chapter Chapter8 { get; set; }

        [JsonProperty(PropertyName = "9")]
        public Chapter Chapter9 { get; set; }

        [JsonProperty(PropertyName = "10")]
        public Chapter Chapter10 { get; set; }

        [JsonProperty(PropertyName = "11")]
        public Chapter Chapter11 { get; set; }

        [JsonProperty(PropertyName = "12")]
        public Chapter Chapter12 { get; set; }

        [JsonProperty(PropertyName = "13")]
        public Chapter Chapter13 { get; set; }

        [JsonProperty(PropertyName = "14")]
        public Chapter Chapter14 { get; set; }

        [JsonProperty(PropertyName = "15")]
        public Chapter Chapter15 { get; set; }

        [JsonProperty(PropertyName = "16")]
        public Chapter Chapter16 { get; set; }

        [JsonProperty(PropertyName = "17")]
        public Chapter Chapter17 { get; set; }

        [JsonProperty(PropertyName = "18")]
        public Chapter Chapter18 { get; set; }

        [JsonProperty(PropertyName = "19")]
        public Chapter Chapter19 { get; set; }

        [JsonProperty(PropertyName = "20")]
        public Chapter Chapter20 { get; set; }

        [JsonProperty(PropertyName = "21")]
        public Chapter Chapter21 { get; set; }

        [JsonProperty(PropertyName = "22")]
        public Chapter Chapter22 { get; set; }

        [JsonProperty(PropertyName = "23")]
        public Chapter Chapter23 { get; set; }

        [JsonProperty(PropertyName = "24")]
        public Chapter Chapter24 { get; set; }

        [JsonProperty(PropertyName = "25")]
        public Chapter Chapter25 { get; set; }

        [JsonProperty(PropertyName = "26")]
        public Chapter Chapter26 { get; set; }

        [JsonProperty(PropertyName = "27")]
        public Chapter Chapter27 { get; set; }

        [JsonProperty(PropertyName = "28")]
        public Chapter Chapter28 { get; set; }

        [JsonProperty(PropertyName = "29")]
        public Chapter Chapter29 { get; set; }

        [JsonProperty(PropertyName = "30")]
        public Chapter Chapter30 { get; set; }
    }
}