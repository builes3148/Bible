﻿using Newtonsoft.Json;

namespace Bible.Models
{
    public class Chapter
    {
        [JsonProperty(PropertyName = "1")]
        public BaseVerse Verse1 { get; set; }

        [JsonProperty(PropertyName = "2")]
        public BaseVerse Verse2 { get; set; }

        [JsonProperty(PropertyName = "3")]
        public BaseVerse Verse3 { get; set; }

        [JsonProperty(PropertyName = "4")]
        public BaseVerse Verse4 { get; set; }

        [JsonProperty(PropertyName = "5")]
        public BaseVerse Verse5 { get; set; }

        [JsonProperty(PropertyName = "6")]
        public BaseVerse Verse6 { get; set; }

        [JsonProperty(PropertyName = "7")]
        public BaseVerse Verse7 { get; set; }

        [JsonProperty(PropertyName = "8")]
        public BaseVerse Verse8 { get; set; }

        [JsonProperty(PropertyName = "9")]
        public BaseVerse Verse9 { get; set; }

        [JsonProperty(PropertyName = "10")]
        public BaseVerse Verse10 { get; set; }

        [JsonProperty(PropertyName = "11")]
        public BaseVerse Verse11 { get; set; }

        [JsonProperty(PropertyName = "12")]
        public BaseVerse Verse12 { get; set; }

        [JsonProperty(PropertyName = "13")]
        public BaseVerse Verse13 { get; set; }

        [JsonProperty(PropertyName = "14")]
        public BaseVerse Verse14 { get; set; }

        [JsonProperty(PropertyName = "15")]
        public BaseVerse Verse15 { get; set; }

        [JsonProperty(PropertyName = "16")]
        public BaseVerse Verse16 { get; set; }

        [JsonProperty(PropertyName = "17")]
        public BaseVerse Verse17 { get; set; }

        [JsonProperty(PropertyName = "18")]
        public BaseVerse Verse18 { get; set; }

        [JsonProperty(PropertyName = "19")]
        public BaseVerse Verse19 { get; set; }

        [JsonProperty(PropertyName = "20")]
        public BaseVerse Verse20 { get; set; }

        [JsonProperty(PropertyName = "21")]
        public BaseVerse Verse21 { get; set; }

        [JsonProperty(PropertyName = "22")]
        public BaseVerse Verse22 { get; set; }

        [JsonProperty(PropertyName = "23")]
        public BaseVerse Verse23 { get; set; }

        [JsonProperty(PropertyName = "24")]
        public BaseVerse Verse24 { get; set; }

        [JsonProperty(PropertyName = "25")]
        public BaseVerse Verse25 { get; set; }

        [JsonProperty(PropertyName = "26")]
        public BaseVerse Verse26 { get; set; }

        [JsonProperty(PropertyName = "27")]
        public BaseVerse Verse27 { get; set; }

        [JsonProperty(PropertyName = "28")]
        public BaseVerse Verse28 { get; set; }

        [JsonProperty(PropertyName = "29")]
        public BaseVerse Verse29 { get; set; }

        [JsonProperty(PropertyName = "30")]
        public BaseVerse Verse30 { get; set; }

        [JsonProperty(PropertyName = "31")]
        public BaseVerse Verse31 { get; set; }

        [JsonProperty(PropertyName = "32")]
        public BaseVerse Verse32 { get; set; }

        [JsonProperty(PropertyName = "33")]
        public BaseVerse Verse33 { get; set; }

        [JsonProperty(PropertyName = "34")]
        public BaseVerse Verse34 { get; set; }

        [JsonProperty(PropertyName = "35")]
        public BaseVerse Verse35 { get; set; }

        [JsonProperty(PropertyName = "36")]
        public BaseVerse Verse36 { get; set; }

        [JsonProperty(PropertyName = "37")]
        public BaseVerse Verse37 { get; set; }

        [JsonProperty(PropertyName = "38")]
        public BaseVerse Verse38 { get; set; }

        [JsonProperty(PropertyName = "39")]
        public BaseVerse Verse39 { get; set; }

        [JsonProperty(PropertyName = "40")]
        public BaseVerse Verse40 { get; set; }
    }
}