﻿namespace Bible.Models
{
    using Models.BiblesType;
    using Newtonsoft.Json;

    public class Bibles
    {
        [JsonProperty(PropertyName = "kjv")]
        public Kjv Kjv { get; set; }

        [JsonProperty(PropertyName = "kjv_strongs")]
        public KjvStrongs KjvStrongs { get; set; }

        [JsonProperty(PropertyName = "tyndale")]
        public Tyndale Tyndale { get; set; }

        [JsonProperty(PropertyName = "coverdale")]
        public Coverdale Coverdale { get; set; }

        [JsonProperty(PropertyName = "bishops")]
        public Bishops Bishops { get; set; }

        [JsonProperty(PropertyName = "geneva")]
        public Geneva Geneva { get; set; }

        [JsonProperty(PropertyName = "tr")]
        public Tr Tr { get; set; }

        [JsonProperty(PropertyName = "trparsed")]
        public Trparsed Trparsed { get; set; }

        [JsonProperty(PropertyName = "rv_1858")]
        public Rv1858 Rv1858 { get; set; }

        [JsonProperty(PropertyName = "rv_1909")]
        public Rv1909 Rv1909 { get; set; }

        [JsonProperty(PropertyName = "sagradas")]
        public Sagradas Sagradas { get; set; }

        [JsonProperty(PropertyName = "rvg")]
        public Rvg Rvg { get; set; }

        [JsonProperty(PropertyName = "martin")]
        public Martin Martin { get; set; }

        [JsonProperty(PropertyName = "epee")]
        public Epee Epee { get; set; }

        [JsonProperty(PropertyName = "oster")]
        public Oster Oster { get; set; }

        [JsonProperty(PropertyName = "afri")]
        public Afri Afri { get; set; }

        [JsonProperty(PropertyName = "svd")]
        public Svd Svd { get; set; }

        [JsonProperty(PropertyName = "bkr")]
        public Bkr Bkr { get; set; }

        [JsonProperty(PropertyName = "stve")]
        public Stve Stve { get; set; }

        [JsonProperty(PropertyName = "finn")]
        public Finn Finn { get; set; }

        [JsonProperty(PropertyName = "luther")]
        public Luther Luther { get; set; }

        [JsonProperty(PropertyName = "diodati")]
        public Diodati Diodati { get; set; }

        [JsonProperty(PropertyName = "synodal")]
        public Synodal Synodal { get; set; }

        [JsonProperty(PropertyName = "karoli")]
        public Karoli Karoli { get; set; }

        [JsonProperty(PropertyName = "lith")]
        public Lith Lith { get; set; }

        [JsonProperty(PropertyName = "maori")]
        public Maori Maori { get; set; }

        [JsonProperty(PropertyName = "cornilescu")]
        public Cornilescu Cornilescu { get; set; }

        [JsonProperty(PropertyName = "thaikjv")]
        public Thaikjv Thaikjv { get; set; }

        [JsonProperty(PropertyName = "wlc")]
        public Wlc Wlc { get; set; }
    }
}