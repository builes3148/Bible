﻿namespace Bible.Models
{
    using Newtonsoft.Json;

    public class Book
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "shortname")]
        public string ShortName { get; set; }
    }
}