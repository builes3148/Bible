﻿namespace Bible.Models
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public class Content
    {
        [JsonProperty(PropertyName = "error_level")]
        public int ErrorLevel { get; set; }

        [JsonProperty(PropertyName = "results")]
        public List<ContentResult> ContentResults { get; set; }
    }
}