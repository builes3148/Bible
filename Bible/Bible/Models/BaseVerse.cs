﻿namespace Bible.Models
{
    using Newtonsoft.Json;

    public class BaseVerse
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "book")]
        public int Book { get; set; }

        [JsonProperty(PropertyName = "chapter")]
        public int Chapter { get; set; }

        [JsonProperty(PropertyName = "verse")]
        public int Verse { get; set; }

        [JsonProperty(PropertyName = "text")]
        public string Text { get; set; }

        [JsonProperty(PropertyName = "italics")]
        public string Italics { get; set; }
    }
}